<?php

/**
 * Configuration.
 * Can also be set via a `cleaninsights.ini` file in the same directory.
 * See example ini file for further documentation.
 */
$TOKEN_AUTH = '';

// Cross-Origin Resource Sharing support. Add here all domains exactly as they
// are sent by the browser in the 'Origin' header. Keep empty to disable.
// Example: 'https://example.com:3000'
$CORS = [];

$MATOMO_BASE_URL = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];

// Number of created tracking requests to send to Matomo at once.
$CHUNK_SIZE = 1000;

// Number of seconds to wait before retry, if Matomo responded with an error.
$DELAY_AFTER_FAILURE = 10;

// Number of maximum retries on error.
$MAX_ATTEMPTS = 3;

$DEBUG = false;


// ********** end of configuration **********

// Override configuration with data from a configuration file, if available.

$CONF_FILE = 'cleaninsights.ini';

if (is_readable($CONF_FILE)) {
    $conf = parse_ini_file($CONF_FILE);

    if (array_key_exists('token_auth', $conf) && $conf['token_auth']) {
        $TOKEN_AUTH = $conf['token_auth'];
    }

    if (array_key_exists('cors', $conf) && $conf['cors']) {
        if (is_array($conf['cors'])) {
            $CORS = $conf['cors'];
        }
        else {
            $CORS = [$conf['cors']];
        }
    }

    if (array_key_exists('matomo_base_url', $conf) && $conf['matomo_base_url']) {
        $MATOMO_BASE_URL = $conf['matomo_base_url'];
    }

    if (array_key_exists('chunk_size', $conf) && $conf['chunk_size']) {
        $CHUNK_SIZE = $conf['chunk_size'];
    }

    if (array_key_exists('delay_after_failure', $conf) && $conf['delay_after_failure']) {
        $DELAY_AFTER_FAILURE = $conf['delay_after_failure'];
    }

    if (array_key_exists('max_attempts', $conf) && $conf['max_attempts']) {
        $MAX_ATTEMPTS = $conf['max_attempts'];
    }

    if (array_key_exists('debug', $conf)) {
        $DEBUG = !!$conf['debug'];
    }
}

// Set up log.

if ($DEBUG) {
    ini_set('log_errors', 1);
    ini_set('error_log', 'ciapi.log');
}

$origin = array_key_exists( 'HTTP_ORIGIN', $_SERVER) ? $_SERVER['HTTP_ORIGIN'] : '';

// Safari needs this in both, an OPTIONS request and the subsequent POST request.
if (in_array($origin, $CORS, true)) {
    header("Access-Control-Allow-Origin: $origin");
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Content-Type');
}

// CORS preflight request support.
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    if (in_array($origin, $CORS, true)) {
        debug("CORS preflight request allowed for '$origin'.");

        http_response_code(200);
    }
    else {
        debug("CORS preflight request denied for '$origin'.");

        http_response_code(403);
    }

    exit(1);
}

// Only allowing POST here.
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    debug('Method not allowed');
    http_response_code(405);
    exit(1);
}

if (!$TOKEN_AUTH) {
    debug('TOKEN_AUTH not configured. Timestamps can not be set correctly without!');
    http_response_code(500);
    exit(1);
}

$json = file_get_contents('php://input');
$data = json_decode($json);

// Obviously malformed POST body.
if (!$data || gettype($data) !== 'object' || !property_exists($data, 'idsite') || $data->idsite < 1) {
    debug('Malformed body. Last JSON error:' . json_last_error() . ' ' . json_last_error_msg());
    http_response_code(400);
    exit(1);
}


// Build output for Matomo Tracking API and send in chunks.

$output = [];

$reporting_start = time();
$reporting_end = 0;
$count = 0;
$result = false;

if (property_exists($data, 'visits') && gettype($data->visits) === 'array') {
    foreach ($data->visits as $visit) {
        $count += renderVisit($visit);
    }
}

if (property_exists($data, 'events') && gettype($data->events) === 'array') {
    foreach ($data->events as $event) {
        $count += renderEvent($event);
    }
}

// Send the rest.
send(false);

debug('result = ' . ($result ? 'success': 'failure') . ", count = $count");

if ($result === false) {
    debug('There were no events and visits or the Matomo Tracking API refused all requests.');
    http_response_code(400);
    exit(1);
}


// Instruct Matomo to re-analyse the complete period of this request
// on the next auto-archiving run.

$start = new DateTime();
$start->setTimestamp($reporting_start);

$end = new DateTime();
$end->setTimestamp($reporting_end);

$query = http_build_query([
    'module' => 'API',
    'method' => 'CoreAdminHome.invalidateArchivedReports',
    'idSites' => $data->idsite,
    'period' => 'range',
    'dates' => $start->format('Y-m-d') . ',' . $end->format('Y-m-d'),
    'token_auth' => $TOKEN_AUTH,
]);

$result = callMatomo(null, $query);

if ($result === false) {
    debug('Matomo could not be triggered to reprocess data!');
}
else {
    debug('Matomo was triggered to reprocess data!');
}

http_response_code(204);

exit();


// Helper functions.

function renderVisit(stdClass $visit): int {
    $action_name = $visit->action_name;

    return render($visit->period_start, $visit->period_end, $visit->times,
        function() use ($action_name) {
        return ['action_name' => $action_name];
    });
}

function renderEvent(stdClass $event): int {
    return render($event->period_start, $event->period_end, $event->times,
        function() use ($event) {
        $e = ['e_c' => $event->category, 'e_a' => $event->action];

        if (property_exists($event, 'name') && $event->name) {
            $e['e_n'] = $event->name;
        }

        if (property_exists($event, 'value') && $event->value) {
            $e['e_v'] = $event->value;
        }

        return $e;
    });
}

function render(int $period_start, int $period_end, int $times, callable $callback): int {
    global $reporting_start, $reporting_end, $output;

    $reporting_start = min($reporting_start, $period_start);
    $reporting_end = max($reporting_end, $period_end);

    $interval = ($period_end - $period_start) / $times;

    // Offset the first record by half an interval, so records are neatly "centered" in the period.
    $period_start += $interval / 2;

    for ($i = 0; $i < $times; $i++) {
        $request = $callback();

        $request['cdt'] = $period_start + $interval * $i;

        $output[] = buildRequest($request);

        send();
    }

    return $times;
}

function buildRequest(array &$request): string {
    global $data;

    $request['idsite'] = $data->idsite;
    $request['rec'] = 1;
    if (property_exists($data, 'lang') && $data->lang) $request['lang'] = $data->lang;
    if (property_exists($data, 'ua') && $data->ua) $request['ua'] = $data->ua;

    return '?' . http_build_query($request);
}

function callMatomo(string $path = null, string $query = null, string $body = null): string {
    global $MATOMO_BASE_URL, $MAX_ATTEMPTS, $DELAY_AFTER_FAILURE;

    $options = [
        'method' => 'POST',
        'header' => 'Content-Type: application/json; charset=UTF-8'];

    if (isset($body)) {
        $options['content'] = $body;
    }

    $context = stream_context_create(['http' => $options]);

    $url = "$MATOMO_BASE_URL/";

    if (isset($path)) {
        $url .= $path;
    }

    if (isset($query)) {
        $url .= "?$query";
    }

    $counter = 1;

    debug("Call Matomo at $url");

    do {
        $result = file_get_contents($url, false, $context);

        $counter++;

        if ($result !== false || $counter > $MAX_ATTEMPTS) {
            break;
        }

        debug("Request error. Sleep $DELAY_AFTER_FAILURE seconds and retry!");

        sleep($DELAY_AFTER_FAILURE);

    } while (true);

    return $result;
}

function sendToMatomo(array &$output): bool {
    global $TOKEN_AUTH;

    $body = json_encode([
        'token_auth' => $TOKEN_AUTH,
        'requests' => $output]);

    if ($body === false) {
        return false;
    }

    $result = callMatomo("matomo.php", null, $body);

    debug("Called tracking API " . (!$result ? 'un' : '') . "successfully with "
        . count($output) . " lines of data: " . substr($body, 0, 256));

    return !!$result;
}

function send(bool $onlyIfChunkSizeReached = true) {
    global $output, $CHUNK_SIZE, $result;

    if (!$onlyIfChunkSizeReached || count($output) >= $CHUNK_SIZE) {
        if (sendToMatomo($output)) {
            // If at least one request succeeded, we consider this good.
            // Otherwise, the client would re-send already processed data.
            $result = true;
        }

        // Remove already sent requests from memory again.
        $output = [];
    }
}

function debug($message) {
    global $DEBUG;
    if (!$DEBUG) return;

    error_log(print_r($message, true));
}