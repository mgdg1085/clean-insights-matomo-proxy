# Clean Insights Matomo Proxy (CIMP)

API to receive Clean Insights SDK data and preprocess it for Matomo's tracking API.

https://developer.matomo.org/api-reference/tracking-api

Goals:
- Clean Insights provides data which is older than a day. For this to work
  with Matomo, we need to use the `token_auth`. But this is way to powerful
  to hand it out to clients. CIMP will keep that around.
  
- Clean Insights aggregates data over a period. To have useful reports
  in Matomo, that aggregated data needs to be pushed into Matomo as single
  data points. CIMP will do that, so the different Clean Insights SDKs don't
  need to implement that multiple times.
  
- When doing bulk uploading to the Matomo tracking API, a lot of data is repeated
  again and again, although it almost never changes. CIMP will take care of this
  and therefore reduce transfer size.
  
- Privacy preservation: Matomo records IP addresses and evaluates different HTTP
  headers. By using the proxy, we make sure, that Matomo isn't ever able to 
  accidentally record this information from a user. 
  
## Compatibility

Development is done against Matomo 3.14.1 and later. However it *should* work with
all Matomo (and Piwik) versions supporting the tracking API v1.

## API

There is only one endpoint which can only be talked to via a POST request:

```json
POST https://matomo.example.org/ci/cleaninsights.php

{
    "idsite": 1,
    "lang": "en_US", // OPTIONAL
    "ua", "iPhone", // OPTIONAL
    "visits": [ // OPTIONAL
        {
            "action_name": "Main/Settings/Some Setting",
            "period_start": 1599041128, // UNIX epoch timestamp
            "period_end": 1601633128, // UNIX epoch timestamp
            "times": 123,
        }
    ],
    "events": [ // OPTIONAL
        {
            "category": "video-content",
            "action": "play",
            "period_start": 1599041128, // UNIX epoch timestamp
            "period_end": 1601633128, // UNIX epoch timestamp
            "times": 12,
        }
    ]
}
```

Note: You will need to provide at least a visit or an event, otherwise nothing
gets recorded, although the script will return with 204 anyway.

### CORS support

CIMP supports Cross-Origin Resource Sharing through supporting OPTIONS preflight requests.
Provide all allowed scheme/domain/port tuples in the `$CORS` array at the top of `cleaninsights.php`.
 
### Formal Specification

JSON Scheme specification: https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/cimp.schema.json

Generated JSON Scheme docs: https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/README.md  


## Installation

- 2 Options:

  - *Easy to install* but also **easy to accidentally deinstall** during Matomo updates:
    On your server, copy this project into a subdirectory of the directory where
    Matomo is installed. (In the example above, we use 'ci'...)
  
  - More work but **more stable** install: Create another `VirtualHost` configuration
    on your web server installation, put `cleaninsights.php` in its root folder.
    In `cleaninsights.ini`, change `matomo_base_url` to point to your Matomo installation.
    Please consult the documentation of the web server you are using on how to set up virtual hosts.

- Create a `token_auth`. See https://matomo.org/faq/general/faq_114/

- Copy that token into `cleaninsights.ini` configuration file:

```ini
token_auth = ''
```

- Check the other configuration options documented in the ini file.

## Author

Benjamin Erhart, berhart@netzarchitekten.com 
for the [Guardian Project](https://guardianproject.info).

## License

CIMP is available under the MIT license. See the LICENSE file for more info.
